********************************************************************************
Introduction
********************************************************************************
Came across this problem that if you upload a file which already exists
in the files folder of Drupal; it does not get overwritten.
This module covers the minute gaps of overwriting the files.
If same file is uploaded multiple times in a node or in the different nodes, file
will get overwritten by the functioanlity of code.
The concept is simple if you upload the file which is existing in Drupal files
will get overwritten.
This is applicable in the case of images also.


********************************************************************************
ISSUES
********************************************************************************

Normally no issue should come as per my regression testing. But if occurs then..

Please use the project issue tracker to report problems
http://drupal.org/project/issues/upload_overwrite

Project page:
http://drupal.org/project/upload_overwrite

********************************************************************************
AUTHOR
********************************************************************************
USERNAME: nitin.k
FULLNAME: Nitin Kumar
EMAIL: bargoti.nitin@gmail.com
